<html>
	<head> 
		<title>Change Password</title>
	</head>
	<body>
		<?php
			if($key_error != 1) { 
		?>
			<?php echo validation_errors('<p class="error">'); ?>
			<form action="<?php echo base_url().'forgot_password/retrive/'.$this->uri->segment(3).'/'.$this->uri->segment(4); ?>" method="post">
				<p>
					<label>Password:</label>
					<input type="password" name="password"/>
				</p>
				<p>
					<label>Confirm Password:</label>
					<input type="password" name="confirm_password"/>
				</p>
				<p>
					<input type="submit" name="submit" value="Change Password"/>
				</p>
			</form>
		<?php } else { ?>
			<h2 align="center">Invalid key, please check it again.</h2>
		<?php } ?>
	</body>
</html>