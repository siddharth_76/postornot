<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Users extends REST_Controller 
{   
    /**
    * Users signup service
    * @param request $_POST
    */
	public function signup_post() {
		/* Check for required parameter */
		$object_info = $_POST;
		$required_parameter = array('full_name', 'email', 'password', 'device_type', 'device_id', 'device_token');
		$chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
             $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
             $this->response($resp);
        }

        $_POST['password'] = md5($_POST['password']);
        $_POST['date_created'] = date('Y-m-d H:i:s');

        /* Check for email */
        $check_email = $this->common_model->getRecordCount(USER, array('email' => $_POST['email']));
        if($check_email > 0) {
        	$resp = array('code' => ERROR, 'message' => 'FAILURE', 'response' => 'EMAIL_IS_ALREADY_EXISTS');
        	$this->response($resp);
        }

        /* Add user record */
        if(!empty($_FILES['profile_pic']['name'])) {
            $config['file_name']     = time().$_FILES['profile_pic']['name']; 
            $config['upload_path']   = './uploads/users';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = '2474898';
            $config['max_width']     = '100024';
            $config['max_height']    = '768000';
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('profile_pic')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $img = array('upload_data' => $this->upload->data());
                $_POST['profile_pic'] = $img['upload_data']['file_name'];
            }
        }

        $userId = $this->common_model->addRecords(USER, $_POST);
        if($userId) {
        	/* Get user data */
        	$userData = $this->common_model->getSingleRecordById(USER, array('user_id' => $userId));
        	if(!empty($userData['profile_pic'])) {
                $userData['profile_pic_url'] = base_url().USER_UPLOAD_PATH.$userData['profile_pic'];
            }
            $resp = array('code' => SUCCESS, 'message' => 'SUCCESS', 'response' => array('user_data' => $userData));
        } else {
        	$resp = array('code' => ERROR, 'message' => 'FAILURE');
        }
        $this->response($resp);
	}

    /**
    * Users login service
    * @param request in $_POST
    */
	public function login_post() {
		/* Check for required parameter */
		$object_info = $_POST;
		$required_parameter = array('email', 'password', 'device_type', 'device_id', 'device_token');
		$chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
             $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
             $this->response($resp);
        }

        /* Check for email */
        $check_email = $this->common_model->getRecordCount(USER, array('email' => $_POST['email']));
        if($check_email == 0) {
        	$resp = array('code' => ERROR, 'message' => 'FAILURE', 'response' => array('error' => 'EMAIL_IS_NOT_EXISTS', 'error_label' => 'This email is not exists in our database'));
        	$this->response($resp);
        }

        $check_login = $this->common_model->getSingleRecordById(USER, array('email' => $_POST['email'], 'password' => md5($_POST['password'])));
		if(!empty($check_login)) {
            if(!empty($check_login['profile_pic'])) {
                $check_login['profile_pic_url'] = base_url().USER_UPLOAD_PATH.$check_login['profile_pic'];
            }
            /* Update user device information */
            $condition = array('user_id' => $check_login['user_id']);
            $updateArr = array('device_type' => $_POST['device_type'], 'device_id' => $_POST['device_id'], 'device_token' => $_POST['device_token']);
			$this->common_model->updateRecords(USER, $updateArr, $condition);

            /* append device info to the users array */
            $check_login['device_type'] = $_POST['device_type'];
            $check_login['device_id'] = $_POST['device_id'];
            $check_login['device_token'] = $_POST['device_token'];
            
            $resp = array('code' => SUCCESS, 'message' => 'SUCCESS', 'response' => array('user_data' => $check_login));
		} else {
			$resp = array('code' => ERROR, 'message' => 'FAILURE', 'response' => array('error' => 'INVALID_DETAILS', 'error_label' => 'Password is not correct'));
		}
		$this->response($resp);
	}

    /**
    * Forgot password
    * @param $_POST['email']
    */	
    public function forgot_password_post() {
        /* Check for required parameter */
        $object_info = $_POST;
        $required_parameter = array('email');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
             $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
             $this->response($resp);
        }

        /* Check for email */
        $check_email = $this->common_model->getRecordCount(USER, array('email' => $_POST['email']));
        if($check_email == 0) {
            $resp = array('code' => ERROR, 'message' => 'FAILURE', 'response' => array('error' => 'EMAIL_IS_NOT_EXISTS', 'error_label' => 'This email is not exists in our database'));
            $this->response($resp);
        }

        /* Get user info */
        $userData = $this->common_model->getSingleRecordById(USER, array('email' => $_POST['email']));
        $password_reset_key = substr(md5(time()),rand(7,9),rand(15,25));

        /* Send email */
        $link = base_url().'forgot_password/retrive/'.$userData['user_id'].'/'.$password_reset_key;
        $subject = 'Forgot Password Request';
        $message = 'Dear,<br/>'.ucfirst($userData['full_name']).'<br/><br/>Please click on this link to change your password.<br/><br/>'.$link.REGARD_MESSAGE;

        if(send_mail($message, $subject, $_POST['email'])) {
            /* Update password reset key on user info */
            $condition = array('user_id' => $userData['user_id']);
            $updateArr = array('password_reset_key' => $password_reset_key);
            $this->common_model->updateRecords(USER, $updateArr, $condition); 
            $resp = array('code' => SUCCESS, 'message' => 'SUCCESS', 'response' => array('reset_link' => $link, 'success' => 'EMAIL_SENT', 'success_label' => 'Please check your email.'));
        } else {
            $resp = array('code' => ERROR, 'message' => 'FAILURE', 'response' => array('error' => 'EMAIL_NOT_SENT', 'error_label' => "We're unable to send email right now, please try after some time."));
        }
        $this->response($resp);
    }
}