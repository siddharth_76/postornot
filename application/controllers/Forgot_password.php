<?php
class Forgot_password extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

    /**
    * Retrive password
    * @param $_POST
    */
	public function retrive() {
        $data = array();
        $data['key_error'] = '';
		$userId = $this->uri->segment(3);
        $reset_key = $this->uri->segment(4);
        /* Check for rest key */
        $check_key = $this->common_model->getRecordCount(USER, array('user_id' => $userId, 'password_reset_key' => $reset_key));
        if($check_key == 1) {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required');
            $this->form_validation->set_error_delimiters('<p style="color:#900">', '</p>');
            
            if($this->form_validation->run() == true) {
                /* Change password */
                $condition = array('user_id' => $userId);
                $updateArr = array('password' => md5($_POST['password']));
                $this->common_model->updateRecords(USER, $updateArr, $condition);

                /* update password reset key */
                $resetArr = array('password_reset_key' => '');
                $this->common_model->updateRecords(USER, $resetArr, $condition);

                /* Redirect page */
                $this->session->set_flashdata('success', 'Password has been changed successfully.');
                redirect(base_url().'forgot_password/success');
            }
        } else {
            $data['key_error'] = true;
        }
        $this->load->view('reset_password', $data);
	}

    public function success() {
        $this->load->view('success_password');
    }
}