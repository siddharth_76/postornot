<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model 
{
    function getAllRecords($table)
	{
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
    function getSingleRecordById($table,$conditions)
	{
	   $query = $this->db->get_where($table,$conditions);
	   return $query->row_array();
	}
	 
	function getAllRecordsById($table,$conditions)
	{
	   $query = $this->db->get_where($table,$conditions);
		return $query->result_array();
	}

	function getAllRecordsOrderById($table, $field, $short, $conditions)
	{
	   $this->db->order_by($field, $short);
	   $query = $this->db->get_where($table,$conditions);
	   return $query->result_array();
	}

    function addRecords($table,$post_data)
	{
		$this->db->insert($table,$post_data); 
		return $this->db->insert_id(); 
	}

	function addRecordsReturnId($table,$post_data)
	{
		$this->db->insert($table,$post_data);
		return $this->db->insert_id(); 
	}
	
	function updateRecords($table, $post_data, $where_condition)
	{
		$this->db->where($where_condition);
		$this->db->update($table, $post_data); 
	}
	
	function deleteRecords($table,$where_condition,$condition)
	{		
	    $this->db->where($where_condition,$condition);
		$this->db->delete($table);
	}	
	
	function getPaginateRecords($table, $result, $offset = 0)
	{
		$query = $this->db->get($table,$result,$offset);
	    return $query->result_array();
	}

	function getPaginateRecordsByConditions($table, $result, $offset=0, $condition)
	{
		$query = $this->db->get_where($table, $condition, $result, $offset);
	    return $query->result_array();
	}

	function getPaginateRecordsByLikeConditions($table, $result, $offset=0, $condition, $like_field, $like_value)
	{
		$this->db->like($like_field, $like_value);
		$query = $this->db->get_where($table, $condition, $result, $offset);
	    return $query->result_array();
	}

	function getTotalRecords($table)
	{
		$query = $this->db->get($table);
		return $query->num_rows();
	}
	function getTotalRecordsByIdLike($table, $condition, $like_field, $like_value)
	{
	    $this->db->like($like_field, $like_value);
	    $query = $this->db->get_where($table, $condition);
		return $query->num_rows();
	}
	
	function getPaginateRecordsByCondition($table,$result,$offset=0,$where_condition,$condition)
	{
	    $this->db->where($where_condition,$condition);
		$query = $this->db->get($table,$result,$offset);
	    return $query->result_array();
	}

	function getPaginateRecordsByOrderByCondition($table, $field, $short, $result, $offset=0, $condition)
	{
	    $this->db->where($condition);
	    $this->db->order_by($field, $short);
		$query = $this->db->get($table,$result,$offset);
	    return $query->result_array();
	}

	function getTotalRecordsByCondition($table,$where_condition,$condition)
	{
	    $this->db->where($where_condition,$condition);
		$query = $this->db->get($table);
		return $query->num_rows();
	}
	
	function fetchMaxRecord($table,$field)
	{
		$this->db->select_max($field,'max');
        $query = $this->db->get($table);
		return $query->row_array();	
	}

	function fetchRecordsByOrder($table,$field,$sort)
	{
	    $this->db->order_by($field,$sort);
		$query = $this->db->get($table);
		return $query->result_array();
	}			
			
	function getAllRecordsByLimitId($table,$conditions,$limit)
	{
	    $this->db->limit($limit);
		$query = $this->db->get_where($table,$conditions);
		return $query->result_array();
	}
	
	function getLatestRecords($table,$date,$limit)
	{
	    $this->db->order_by($date,'desc');
	    $this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	function getRelatedRecords($table,$date,$conditions)
	{
	    $this->db->order_by($date,'desc');
	    $this->db->limit(4);
		$query = $this->db->get_where($table,$conditions);
		return $query->result_array();
	}
	
	function getAscLatestRecords($table,$date,$limit)
	{
	    $this->db->order_by($date,'asc');
	    $this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	function getLimitedRecords($table,$limit)
	{
	    $this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result_array();
	}

	function addSetting($table, $post_data)
	{
		$this->db->insert($table, $post_data); 
	}

	function updateSetting($table, $post_data)
	{
		$this->db->update($table, $post_data);
	}

	function getSettings($table, $key)
	{
	    $this->db->where('meta_key', $key);
	    $query = $this->db->get_where($table);
		return $query->row_array();
	}

	function getRecordCount($table, $where_condition)
	{
	    $this->db->where($where_condition);
		$query = $this->db->get($table);
		return $query->num_rows();
	}
}