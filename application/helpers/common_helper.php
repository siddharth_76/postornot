<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('send_mail'))
{
	function send_mail($message, $subject, $email_address)
	{          
	    $ci =&get_instance();
		$ci->load->library('email');
		$config['mailtype']='html';
		$ci->email->initialize($config);	
		$ci->email->from(ADMIN_EMAIL);
		$ci->email->to($email_address);
		$ci->email->subject($subject);
        $ci->email->message($message);
        
		if($ci->email->send()) {	
			return true;
		} else {
			return false;
		}
	}
}

if(!function_exists('p')) {
	function p($array) {
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
}

if(!function_exists('check_required_value')) {
	function check_required_value($chk_params, $converted_array) {
        foreach ($chk_params as $param) {
            if (array_key_exists($param, $converted_array) && ($converted_array[$param] != '')) {
                $check_error = 0;
            } else {
                $check_error = array('check_error' => 1, 'param' => $param);
                break;
            }
        }
        return $check_error;
	}
}
/* End of file common_helper.php */
/* Location: ./system/application/helpers/common_helper.php */